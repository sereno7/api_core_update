﻿using BuildYourTeam.DAL.Repositories;
using MAKERHUB_API_CORE.Services;
using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.SqlClient;

namespace MAKERHUB_API_CORE.Validators
{
    public class NoEventAtSameDate : ValidationAttribute
    {
        public NoEventAtSameDate()
        {
            ErrorMessage = "This Date is already set";
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            if (value == null) return new ValidationResult("This date Is Null");
            DayService dayService = (DayService)context.GetService(typeof(DayService));
            if(!dayService.CheckDate((DateTime)value))
            {
                return null;
            }
            return new ValidationResult("This date Already Used");
        }
    }
}