﻿using MAKERHUB_API_CORE.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Validators
{
    public class NoDoubleTeamNameInSameDay : ValidationAttribute
    {
        public NoDoubleTeamNameInSameDay()
        {
            ErrorMessage = "This team Exist";
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            if (value == null) return new ValidationResult("This team Is Null");
            PersonService personService = (PersonService)context.GetService(typeof(PersonService));
            if (!personService.personExists((string)value))
            {
                return null;
            }
            return new ValidationResult("This team Exist");
        }
    }
}
