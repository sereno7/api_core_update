﻿using BuildYourTeam.ASP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Validators
{
    public class DateValidationAttribute : Attribute, IAuthorizationFilter
    {
        private DateTime today;

        public DateValidationAttribute()
        {
            this.today = DateTime.Now;
            
        }
        public async void OnAuthorization(AuthorizationFilterContext context)
        {

            StreamReader stream = new StreamReader(context.HttpContext.Request.Body);
            string body = await stream.ReadToEndAsync();
            stream.Close();
            body = body.Replace("\\", "").Replace("\"", " ");
            string[] tab = body.Split(" ");
            DateTime d;
            foreach (string t in tab)
            {
                if (DateTime.TryParse(t, out d))
                {
                    if (d.AddDays(1) <= DateTime.Now)
                    {
                        context.Result = new UnauthorizedResult();
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
    }
}
