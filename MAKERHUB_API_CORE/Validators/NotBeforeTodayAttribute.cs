﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MAKERHUB_API_CORE.Validators
{
    public class NotBeforeTodayAttribute : ValidationAttribute
    {

        public NotBeforeTodayAttribute()
        {
            ErrorMessage = "This Date Should be After Today";
        }

        public override bool IsValid(object value)
        {
            if (value == null) return true;
            return (DateTime)value > DateTime.Now;
        }
    }
}