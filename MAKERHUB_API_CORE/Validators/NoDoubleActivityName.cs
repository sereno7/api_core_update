﻿using MAKERHUB_API_CORE.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Validators
{
    public class NoDoubleActivityName : ValidationAttribute
    {
        public NoDoubleActivityName()
        {
            ErrorMessage = "This activity Exist";
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            if (value == null) return new ValidationResult("This activity Is Null");
            ActivityService activityService = (ActivityService)context.GetService(typeof(ActivityService));
            if (!activityService.activiyExist((string)value))
            {
                return null;
            }
            return new ValidationResult("This activity Exist");
        }
    }
}
