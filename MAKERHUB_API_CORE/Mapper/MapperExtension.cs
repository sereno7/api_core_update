﻿using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Entities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BuildYourTeam.ASP.Mapper
{
    public static class MapperExtension
    {
        public static T Mapto<T>(this object from)
            where T : new()
        {
            T result = new T();
            try
            {
                foreach (var p in typeof(T).GetProperties())
                {
                    if (from.GetType().GetProperty(p.Name) != null)
                    {
                        PropertyInfo prop = from.GetType().GetProperty(p.Name);
                        object value = prop.GetValue(from);
                        p.SetValue(result, value);
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e, "Mappeur Error");
                throw e;
            }
        }

        //public static PersonModel ToDto(this Person dto)
        //{
        //    try
        //    {
        //        return new PersonModel
        //        {
        //            Id = dto.Id,
        //            Name = dto.Name,
        //            IdTeams = dto.IdTeams,
        //            Role = dto.Role,
        //            IdDay = dto.IdDay
        //        };
        //    }
        //    catch (Exception e)
        //    {
        //        Log.Error(e, "Mapper Error");
        //        throw e;
        //    }
        //}
    }
}