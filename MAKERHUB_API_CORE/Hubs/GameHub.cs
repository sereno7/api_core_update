﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.ASP.Models;
using MAKERHUB_API_CORE.Filters;
using MAKERHUB_API_CORE.Models;
using MAKERHUB_API_CORE.Models.ModelUtil;
using MAKERHUB_API_CORE.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Serilog;


namespace signalR_testing.Hubs
{
    //[ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GameHub : Hub
    {
        //private bool _roundEnd;

        private LeaderBoardService _service;
        private ActivityService _serviceA;
        private TimerService _serviceTimer;
        private TeamsService _serviceTeam;
        private DayService _serviceDay;
        private readonly IHubContext<GameHub> _hubContext;
        //private static List<TabTeamIsReadyModel> _listTeamReady = new List<TabTeamIsReadyModel>();
        private bool _pauseStatus;

        public GameHub
        (
            LeaderBoardService service,
            ActivityService serviceA,
            TimerService serviceT,
            TeamsService serviceTeam,
            DayService serviceDay,
            //IHubContext => SendASync to client through Task
            IHubContext<GameHub> hubContext
        )
        {
            this._service = service;
            this._serviceA = serviceA;
            this._serviceTimer = serviceT;
            this._serviceTeam = serviceTeam;
            this._serviceDay = serviceDay;
            this._hubContext = hubContext;
            this._pauseStatus = false;
            //this._roundEnd = false;
        }


        // ECRAN TEAM / MASTER => GESTION DES ENREGISTREMENTS SUR L'ACTIVITE EN COURS
        public async Task SendNewTeam(int idT, int idA, bool r)
        {
            try
            {
                //Get first TEAMMODEL register in the same Activity
                TeamsModel team = _serviceTeam.GetById(idT);
                DayModel d = _serviceDay.GetById(team.IdDay);
                TeamsModel teamAlreadyRecord = _serviceTeam.Get().FirstOrDefault(x => x.CurrentActivity == idA);
                if (teamAlreadyRecord != null)
                {
                    await Clients.Caller.SendAsync("Message", $"L'équipe {teamAlreadyRecord.Name} est déjà inscrite ici!");
                    await Clients.Caller.SendAsync("UnregisterFromActivity");
                }

                else if (team.OldActivity == idA)
                {
                    await Clients.Caller.SendAsync("Message", $"Vous ne pouvez pas participer 2 fois à la même épreuve d'affilée");
                    await Clients.Caller.SendAsync("UnregisterFromActivity");
                }
                else
                {
                    //Get new activityName
                    string activityName = _serviceA.GetById(idA).Name;
                    //MAJ TEAMS + PUT
                    team.CurrentActivity = idA;
                    team.IsReady = r;
                    team.IsRegistered = true;
                    _serviceTeam.Put(team);
                    //Get DAY and LIST of all REGISTERED TEAM IN THE Current 
                    IEnumerable<TabTeamIsReadyModel> list = _serviceTeam.GetByCurrentActivity(d.CountActivity);
                    await Clients.All.SendAsync("NewTeam", list.ToList());
                    await Clients.Caller.SendAsync("Message", $"Vous êtes enregistré sur {activityName}");
                }

            }
            catch (Exception e)
            {
                Log.Error(e, "Hub ERROR SendNewTeam");
                await Clients.Caller.SendAsync("Message", "Erreur, veuillez recommencer");
            }
        }

        //SEnd ready
        public async Task SendReady(int idT, int idC)
        {
            TeamsModel team = _serviceTeam.GetById(idT);
            team.IsReady = true;
            _serviceTeam.Put(team);
            List<TabTeamIsReadyModel> liste = _serviceTeam.GetByCurrentActivity(idC).ToList();
            await Clients.Caller.SendAsync("Message", "Vous avez confirmé être prêt");
            await Clients.All.SendAsync("NewTeam", liste);
        }


        //Send unregister
        public async Task Unregister(int idT, int idC)
        {
            TeamsModel team = _serviceTeam.GetById(idT);
            team.CurrentActivity = null;
            team.IsRegistered = false;
            _serviceTeam.Put(team);
            List<TabTeamIsReadyModel> liste = _serviceTeam.GetByCurrentActivity(idC).ToList();
            await Clients.All.SendAsync("NewTeam", liste);
            await Clients.Caller.SendAsync("UnregisterFromActivity");
            await Clients.Caller.SendAsync("Message", "Vous pouvez sélectionner une autre activité");
        }
        

        // DEMANDE UPDATE LISTE TEAM READY
        public async Task updateListTeamReady(int id)
        {
            try
            {
                List<TabTeamIsReadyModel> liste = _serviceTeam.GetByCurrentActivity(id).ToList();
                await Clients.All.SendAsync("NewTeam", liste);
            }
            catch (Exception e)
            {
                Log.Error(e, "Hub ERROR UpdateListTeamReady");
                await Clients.Caller.SendAsync("Message", "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }


        public async Task SendStartGame(int id)
        {
            //UPDATE All isReady to FALSE
            try
            {
                //Change status DAY en 2
                _serviceDay.UpdateStatus(id, 2);
                _serviceTeam.ResetIsRegistered();
                _serviceTeam.ResetIsReady();

                await Clients.All.SendAsync("Started", true);
                await Clients.Caller.SendAsync("Message", "La partie démarre");
            }
            catch (Exception e)
            {
                Log.Error(e, "Hub ERROR SendStartGame");
                await Clients.Caller.SendAsync("Message", "Une erreur inattendue, veuillez recommencer");
            }
        }

        public async Task SendStartTimer()
        {
            try
            {
                this._serviceTimer.roundEnd = false;
                if (!_pauseStatus)
                {
                    if (_serviceTimer.Startimestamp == 0)
                        _serviceTimer.SetStartTimestamp(1);
                    // Task run and signalR continue to listen + excecute
                    _ = Task.Run(async () =>
                    {
                        while (t() <= _serviceTimer.Timer)
                        {
                            long diff = Math.Abs(t() - _serviceTimer.Timer);
                            await _hubContext.Clients.All.SendAsync("Discount", diff);
                            System.Threading.Thread.Sleep(1000);
                        }
                        this._serviceTimer.roundEnd = true;
                        _serviceTimer.SetPauseTimestamp(0);
                    });
                }
               
            }
            catch (Exception e)
            {
                Log.Error(e, "Hub ERROR SendStartTimer");
                await Clients.Caller.SendAsync("Message", "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        private long t()
        {
            return ((_serviceTimer.GetTimestamp() - _serviceTimer.PauseDuration) -_serviceTimer.PauseTotal) -_serviceTimer.Startimestamp;
        }

        public async Task SendPauseAndResume(bool b)
        {
            try
            {
                if (b)
                {
                    _pauseStatus = true;
                    _serviceTimer.SetPauseTimestamp(1);
                    await Clients.Caller.SendAsync("Message", "Partie en pause");
                }

                if (!b)
                {
                    _pauseStatus = false;
                    _serviceTimer.SetPauseTimestamp(0);
                }

                await Clients.All.SendAsync("Pause", b);
                await Clients.Caller.SendAsync("Message", "Reprise de la partie");
            }
            catch (Exception e)
            {
                Log.Error(e, "Hub ERROR SendPauseAndResume");
                await Clients.Caller.SendAsync("Message", "Une erreur inattendue, veuillez recommencer");
            }
        }



        public async Task SendGameOver(int id)
        {
            try
            {
                _serviceTeam.ResetCurrentActivity();
                // Reset Status pause to false
                _pauseStatus = false;
                //Increment ActivityCount
                _serviceDay.Put(id);
                //Update status of day to 1
                _serviceDay.UpdateStatus(id, 1);
                //Reset Timer + Total pause
                _serviceTimer.SetStartTimestamp(0);
                _serviceTimer.ResetPauseTotal();
                //ClearList
                //_listTeamReady.Clear();

                await Clients.All.SendAsync("End");
            }
            catch (Exception e)
            {
                Log.Error(e, "Hub ERROR SendGameOver");
                await Clients.Caller.SendAsync("Message", "Une erreur inattendue, veuillez recommencer");
            }
        }


        public async Task SendScoreSignalR(LeaderBoardModel lb)
        {
            try
            {
                //_serviceTeam.UpdateOldActivity(lb.IdTeam, lb.IdActivity);
                _service.Add(lb);
                IEnumerable<LeaderBoardDetailsByActivityModel> l = _service.GetLeaderboardDetailsByActivityCount(lb.IdDay, lb.ActivityCount).Select(x => x.Mapto<LeaderBoardDetailsByActivityModel>());
                IEnumerable<LeaderBoardDetailsByActivityModel> m = _service.GetLeaderboardDetailsByActivity(lb.IdDay, lb.IdActivity).Select(x => x.Mapto<LeaderBoardDetailsByActivityModel>());
                await Clients.All.SendAsync("getScoreMaster", l.ToList());
                await Clients.Caller.SendAsync("getScore", m.ToList());
            }
            catch (Exception e)
            {
                Log.Error(e, "Hub ERROR SendScoreSignalR");
                await Clients.Caller.SendAsync("Message", "Une erreur inattendue, veuillez recommencer");
            }
        }

        public async Task SendEndDay(int id)
        {
            try
            {
                _serviceDay.UpdateStatus(id, 3);
                await Clients.All.SendAsync("endDay");
            }
            catch (Exception e)
            {
                Log.Error(e, "Hub ERROR enDay");
                await Clients.Caller.SendAsync("Message", "Une erreur inattendue, veuillez recommencer");
            }
        }


        public async Task WhereIamRegistered(int id)
        {
            try
            {
                TeamsModel t = _serviceTeam.GetById(id);
                await Clients.Caller.SendAsync("youRegistered", t.IsRegistered);
            }
            catch (Exception e)
            {
                Log.Error(e, "Hub ERROR WhereIamRegistered");
                await Clients.Caller.SendAsync("Message", "Une erreur inattendue, veuillez recommencer");
            }
        }

        public async Task CheckIfReady(int id)
        {
            try
            {
                TeamsModel t = _serviceTeam.GetById(id);
                await Clients.Caller.SendAsync("checkIfReady", t.IsReady);
            }
            catch (Exception e)
            {
                Log.Error(e, "Hub ERROR CheckIfReady");
                await Clients.Caller.SendAsync("Message", "Une erreur inattendue, veuillez recommencer");
            }
        }

        public async Task RoundIsEnded()
        {
            try
            { 
                await Clients.Caller.SendAsync("roundIsEnded", this._serviceTimer.roundEnd);
            }
            catch (Exception e)
            {
                Log.Error(e, "Hub ERROR roundIsEnded");
                await Clients.Caller.SendAsync("Message", "Une erreur inattendue, veuillez recommencer");
            }
        }

        public async Task ScoreAlreadySended(int idD, int idT, int activityCount)
        {
            try
            {
                IEnumerable<LeaderBoardDetailsByActivityModel> l = _service.GetLeaderboardDetailsByActivityCount(idD, activityCount).Select(x => x.Mapto<LeaderBoardDetailsByActivityModel>());
                var temp = l.FirstOrDefault(x => x.IdTeam == idT);
                bool b = temp != null ? true : false;
                await Clients.Caller.SendAsync("scoreAlreadySended", b);
            }
            catch (Exception e)
            {
                Log.Error(e, "Hub ERROR scoreAlreadySended");
                await Clients.Caller.SendAsync("Message", "Une erreur inattendue, veuillez recommencer");
            }
        }

        public async Task CheckPauseStatus()
        {
            try
            {
                bool b = _serviceTimer.PauseTimestamp == 0 ? false : true;
                await Clients.Caller.SendAsync("CheckPauseStatus", b);
            }
            catch (Exception e)
            {
                Log.Error(e, "Hub ERROR CheckPauseStatus");
                await Clients.Caller.SendAsync("Message", "Une erreur inattendue, veuillez recommencer");
            }
        }

        //public async Task GetScoreMaster(DayModel day)
        //{
        //    try
        //    {
        //        IEnumerable<LeaderBoardDetailsByActivityModel> l = _service.GetLeaderboardDetailsByActivityCount(day.Id, day.CountActivity).Select(x => x.Mapto<LeaderBoardDetailsByActivityModel>());
        //        await Clients.All.SendAsync("getScoreMaster", l.ToList());
        //    }
        //    catch (Exception e)
        //    {
        //        Log.Error(e, "Hub ERROR GetScoreMaster");
        //        await Clients.Caller.SendAsync("Message", "Une erreur inattendue, veuillez recommencer");
        //    }
        //}


    }
}

