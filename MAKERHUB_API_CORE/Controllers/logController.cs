﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MAKERHUB_API_CORE.Filters;
using MAKERHUB_API_CORE.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace MAKERHUB_API_CORE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class logController : ControllerBase
    {
        private LogService _service;

        public logController(LogService service)
        {
            this._service = service;
        }

        [HttpGet]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public IActionResult Get()
        {
            try
            {
                return Ok(this._service.Get());
            }
            catch (Exception e)
            {
                Log.Error(e, "LogController Error GET");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }
    }
}
