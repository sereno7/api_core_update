﻿using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Repositories;
using MAKERHUB_API_CORE.Services;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;


namespace BuildYourTeam.ASP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private PersonService _service;

        public PersonController(PersonService service)
        {
            this._service = service;
        }

        [HttpGet]
        public IEnumerable<PersonModel> Get()
        {
            try
            {
                return this._service.Get();
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                throw e;
            }
        }

    }
}