﻿using BuildYourTeam.ASP.Models;
using MAKERHUB_API_CORE.Filters;
using MAKERHUB_API_CORE.Models;
using MAKERHUB_API_CORE.Services;
using MAKERHUB_API_CORE.Validators;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BuildYourTeam.ASP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DayController : ControllerBase
    {

        private DayService _service;

        public DayController(DayService service)
        {
            this._service = service;
        }
        [HttpDelete("{id:int}")]
        [ApiAuthorize("ADMIN")]
        public IActionResult Delete(int id)
        {
            try
            {
                this._service.Delete(id);
                return Ok();
            }
            catch (Exception e)
            {
                Log.Error(e, "DayController Error POST");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }


        [HttpGet("{id:int}")]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public IActionResult GetById(int id)
        {
            try
            {
                DayModel d = this._service.GetById(id);
                if (d != null)
                    return Ok(d);
                else
                   return BadRequest("L'évènement n'existe pas");
            }
            catch (Exception e)
            {
                Log.Error(e, "DayController Error GET");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }


        [HttpGet]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public IActionResult Get()
        {
            try
            {
                return Ok(this._service.Get().ToList());
            }
            catch (Exception e)
            {
                Log.Error(e, "DayController Error GET");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpPost]
        [ApiAuthorize("ADMIN")]
        public IActionResult Add(DayModel d)
        {
            return Ok(this._service.Post(d));
        }

        [HttpPut]
        [ApiAuthorize("ADMIN")]
        public IActionResult Update(DayUpdateModel d)
        {
            try
            {
                
                this._service.Update(d);
                return Ok();
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                throw e;
            }
        }

        //[HttpPut]
        //[Route("UpdateStatus/")]
        //public void UpdateStatus([FromBody] UpdateDayModel t)
        //{
        //    try
        //    {
        //        this._service.UpdateStatus(t.IdD, t.IdS);
        //    }
        //    catch (Exception e)
        //    {
        //        Log.Error(e.Message);
        //        throw e;
        //    }
        //}

        [HttpGet("checkdate/{id}")]
        [ApiAuthorize("ADMIN")]
        public IActionResult CheckDate([FromRoute] int id, [FromQuery] DateTime? date)
        {
            if (date != null)
                return Ok(this._service.CheckDate(date, id));
            return Ok(false);
        }



    }
}