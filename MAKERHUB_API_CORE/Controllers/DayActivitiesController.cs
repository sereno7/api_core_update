﻿using System;
using System.Collections.Generic;
using System.Linq;
using MAKERHUB_API_CORE.Filters;
using MAKERHUB_API_CORE.Models;
using MAKERHUB_API_CORE.Services;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace MAKERHUB_API_CORE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DayActivitiesController : ControllerBase
    {
        private DayActivitiesService _service;

        public DayActivitiesController(DayActivitiesService service)
        {
            this._service = service;
        }

        [HttpGet]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<List<DayActivitiesModel>> Get()
        {
            try
            {
                return this._service.GetAll().ToList();

            }
            catch (Exception e)
            {
                Log.Error(e, "DayActivitiesController Error GET");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet("{id:int}")]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<List<DayActivitiesModel>> GetByIdDay(int id)
        {
            try
            {
                return this._service.GetByIdDay(id).ToList();

            }
            catch (Exception e)
            {
                Log.Error(e, "DayActivitiesController Error GetByIdDay");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpPost]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult Add(DayActivitiesModel model)
        {
            try
            {
                this._service.Add(model);
                return Ok();
            }
            catch (Exception e)
            {
                Log.Error(e, "DayActivitiesController Error ADD");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpPost]
        [Route("delete/")]
        [ApiAuthorize("ADMIN")]
        public ActionResult delete(DayActivitiesModel a)
        {
            try
            {
                this._service.DeleteByDetails(a.IdActivity, a.IdDay);
                return Ok();
            }
            catch (Exception e)
            {
                Log.Error(e, "DayActivitiesController Error DELETE");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

    }
}