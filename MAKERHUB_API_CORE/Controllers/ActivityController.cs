﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using MAKERHUB_API_CORE.Filters;
using MAKERHUB_API_CORE.Models;
using MAKERHUB_API_CORE.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace MAKERHUB_API_CORE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class ActivityController : ControllerBase
    {
        private ActivityService _service;


        public ActivityController(ActivityService service)
        {
            this._service = service;
        }

        [HttpGet]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<List<ActivityModel>> Get()
        {
            try
            {
                return this._service.GetActivityOrderById().ToList();
            }
            catch (Exception e)
            {
                Log.Error(e,"ActivityController Error GET");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }


        [HttpGet("{id:int}")]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<ActivityModel> GetById(int id)
        {
            try
            {
                ActivityModel temp = this._service.GetById(id);
                if(temp != null)
                {
                    return this._service.GetById(id);
                }
                else
                {
                    return BadRequest("L'activité n'existe pas");
                }
            }
            catch (Exception e)
            {
                Log.Error(e,"ActivityController Error GET");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        //[HttpGet("{id:int}")]
        //public ActionResult<List<ActivityModel>> GetActivityByIdDay(int id)
        //{
        //    try
        //    {
        //            return this._service.GetActivityByIdDay(id);

        //    }
        //    catch (Exception e)
        //    {
        //        Log.Error(e, "ActivityController Error GET");
        //        return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
        //    }
        //}

        [HttpPost]
        [ApiAuthorize("ADMIN")]
        public ActionResult Add(ActivityModel model)
        {
            try
            {
                this._service.Add(model);
                return Ok();
            }
            catch (Exception e)
            {
                Log.Error(e,"ActivityController Error ADD");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }
        [HttpDelete("{id:int}")]
        [ApiAuthorize("ADMIN")]
        public ActionResult Delete(int id)
        {
            try
            {
                this._service.Delete(id);
                return Ok();
            }
            catch (Exception e)
            {
                Log.Error(e, "ActivityController Error DELETE");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet("checkactivity/")]
        [ApiAuthorize("ADMIN")]
        public IActionResult activiyExist([FromQuery]string name)
        {
            try
            {
                return Ok(this._service.activiyExist(name));
            }
            catch (Exception e)
            {
                Log.Error(e, "ActivityController Error activiyExist");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet("candelete/{id:int}")]
        [ApiAuthorize("ADMIN")]
        public IActionResult ActivityAlreadyUsed(int id)
        {
            try
            {
                return Ok(this._service.ActivityAlreadyUsed(id));
            }
            catch (Exception e)
            {
                Log.Error(e, "ActivityController Error ActivityAlreadyUsed");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }
    }
}