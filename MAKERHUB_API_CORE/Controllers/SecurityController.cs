﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.PeerToPeer.Collaboration;
using System.Threading.Tasks;
using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using MAKERHUB_API_CORE.Filters;
using MAKERHUB_API_CORE.Security;
using MAKERHUB_API_CORE.Services;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog;

namespace MAKERHUB_API_CORE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : ControllerBase
    {
        private SecurityService security;

        public SecurityController(SecurityService security)
        {
            this.security = security;
        }

        [HttpPost("login")]
        [Produces("application/json", Type = typeof(string))]
        public IActionResult Login(LoginModel dto)
        {
            try
            {
                string token = security.login(dto);
                if (token != null)
                    return Ok(token);
                else
                    return BadRequest("L'utilisateur n'existe pas");
            }
            catch (Exception e)
            {
                Log.Error(e, "SecurityController Error LOGIN");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet("logoff/{id:int}")]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult logoff(int id)
        {
            try
            {
                security.logoff(id);
                return Ok();
            }
            catch (Exception e)
            {
                Log.Error(e,"SecurityController Error LOGOFF");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }


        //[HttpGet("getpublickey")]
        //public IActionResult GetPublicKey()
        //{
        //    this.security._keyGenerator.GenerateKeys(RSAKeySize.Key2048);
        //    string publicKey = this.security._keyGenerator.PublicKey;

        //    if (publicKey != null)
        //        return Ok(publicKey);
        //    else
        //        return NotFound();
        //}


    }
}
