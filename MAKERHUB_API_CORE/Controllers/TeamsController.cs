﻿using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Repositories;
using MAKERHUB_API_CORE.Filters;
using MAKERHUB_API_CORE.Services;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BuildYourTeam.ASP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {

        private TeamsService _service;

        public TeamsController(TeamsService service)
        {
            this._service = service;
        }

        // GET: Day
        [HttpGet]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<List<TeamsModel>> Get()
        {
            try
            {
                return this._service.Get().ToList();
            }
            catch (Exception e)
            {
                Log.Error(e, "TeamsController Error GET");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet("{id:int}")]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<TeamsModel> GetById(int id)
        {
            try
            {
                TeamsModel team = this._service.GetById(id);
                if (team != null)
                    return team;
                else
                    return BadRequest("La team n'existe pas");
            }
            catch (Exception e)
            {
                Log.Error(e, "TeamsController Error GET");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpPut]
        [ApiAuthorize("ADMIN")]
        public ActionResult put(TeamsModel t)
        {
            try
            {
                this._service.Put(t);
                return Ok();
            }
            catch (Exception e)
            {
                Log.Error(e, "TeamsController Error PUT");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpPost]
        [ApiAuthorize("ADMIN")]
        public ActionResult Add(TeamsModel t)
        {
            try
            {
                if(!this._service.Add(t))
                    return Ok();
                return BadRequest("La team existe déjà");
            }
            catch (Exception e)
            {
                Log.Error(e, "TeamsController Error POST");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpDelete("{id:int}")]
        [ApiAuthorize("ADMIN")]
        public ActionResult Delete(int id)
        {
            try
            {
                this._service.Delete(id);
                return Ok(true);
            }
            catch (Exception e)
            {
                Log.Error(e, "TeamsController Error DELETE");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        //[HttpGet("checkteam/{id}")]
        //[ApiAuthorize("ADMIN")]
        //public IActionResult teamExist([FromRoute] int id, [FromQuery] string name)
        //{
        //    try
        //    {
        //        return Ok(this._service.teamExist(name, id));
        //    }
        //    catch (Exception e)
        //    {
        //        Log.Error(e, "ActivityController Error activiyExist");
        //        return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
        //    }
        //}
        [HttpGet("checkteam")]
        [ApiAuthorize("ADMIN")]
        public IActionResult teamExist([FromQuery] string name)
        {
            try
            {
                return Ok(this._service.teamExist(name));
            }
            catch (Exception e)
            {
                Log.Error(e, "ActivityController Error activiyExist");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }
    }
}