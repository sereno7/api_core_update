﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MAKERHUB_API_CORE.Filters;
using MAKERHUB_API_CORE.Models;
using MAKERHUB_API_CORE.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace MAKERHUB_API_CORE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LeaderBoardController : ControllerBase
    {
        private LeaderBoardService _service;

        public LeaderBoardController(LeaderBoardService service)
        {
            this._service = service;
        }

        [HttpGet]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<List<LeaderBoardModel>> Get()
        {
            try
            {
                return this._service.GetAll().ToList();
            }
            catch (Exception e)
            {
                Log.Error(e, "LeaderboardController Error GET");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet("{id:int}")]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<List<LeaderBoardDetailsModel>> Get(int id)
        {
            try
            {
                return this._service.GetByIdActity(id).ToList();
   
            }
            catch (Exception e)
            {
                Log.Error(e, "LeaderboardController Error GET");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet]
        [Route("GetTotalById/{idD:int}")]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<List<LeaderBoardTotalByTeamModel>> GetTotalById(int idD)
        {
            try
            {
                return this._service.GetLeaderBoardTotalByTeam(idD).ToList();

                    
            }
            catch (Exception e)
            {
                Log.Error(e, "LeaderboardController Error GET");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet]
        [Route("getaverage/")]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<List<ActivitiesAverageModel>> GetAverage()
        {
            try
            {
                return this._service.GetAverage().ToList();


            }
            catch (Exception e)
            {
                Log.Error(e, "LeaderboardController Error GET AVERAGE");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet]
        [Route("getmin/")]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<List<ActivitiesMinModel>> GetMin()
        {
            try
            {
                return this._service.GetMin().ToList();


            }
            catch (Exception e)
            {
                Log.Error(e, "LeaderboardController Error GET MIN");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet]
        [Route("getmax/")]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<List<ActivitiesMaxModel>> GetMax()
        {
            try
            {
                return this._service.GetMax().ToList();


            }
            catch (Exception e)
            {
                Log.Error(e, "LeaderboardController Error GET MAX");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet]
        [Route("GetMaxByActivity/{idD:int}/{idA:int}")]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<List<LeaderBoardDetailsByActivityModel>> GetLeaderboardDetailsByActivity(int idD, int idA)
        {
            try
            {
                return this._service.GetLeaderboardDetailsByActivity(idD, idA).ToList();
            }
            catch (Exception e)
            {
                Log.Error(e, "LeaderboardController Error");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet]
        [Route("GetMaxByActivityCount/{idD:int}/{idC:int}")]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult<List<LeaderBoardDetailsByActivityModel>> GetLeaderboardDetailsByActivityCount(int idD, int idC)
        {
            try
            {
                return this._service.GetLeaderboardDetailsByActivityCount(idD, idC).ToList();
            }
            catch (Exception e)
            {
                Log.Error(e, "LeaderboardController Error");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }


        [HttpPost]
        [ApiAuthorize("ADMIN", "ANIMATOR", "MASTER")]
        public ActionResult Add(LeaderBoardModel model)
        {
            try
            {
                this._service.Add(model);
                return Ok();
            }
            catch (Exception e)
            {
                Log.Error(e, "LeaderboardController Error ADD");
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }
    }
}