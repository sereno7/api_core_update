﻿using MAKERHUB_API_CORE.Validators;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Models
{
    public class ActivityModel
    {

        public int? Id { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        [NoDoubleActivityName]
        public string Name { get; set; }


        [MinLength(3)]
        [MaxLength(255)]
        public string Description { get; set; }
    }
}
