﻿

using MAKERHUB_API_CORE.Validators;
using System.ComponentModel.DataAnnotations;

namespace BuildYourTeam.ASP.Models
{
    public class LoginModel
    {
        [Required]
        [MaxLength(255)]
        public string Login { get; set; }

        [Required]
        [MaxLength(255)]
        public string PlainPassword { get; set; }
    }
}