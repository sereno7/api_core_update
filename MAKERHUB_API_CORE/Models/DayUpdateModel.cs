﻿using BuildYourTeam.ASP.Models;
using MAKERHUB_API_CORE.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Models
{
    public class DayUpdateModel
    {
        public int Id { get; set; }
        [Required]
        public string CustomerName { get; set; }

        [NotBeforeToday]
        [Required]
        public DateTime Date { get; set; }
        public int Status { get; set; }

        //public TeamsModel NewTeam { get; set; }

        //public IEnumerable<TeamsModel> listTeams { get; set; }

        //public int CountActivity { get; set; }
    }
}
