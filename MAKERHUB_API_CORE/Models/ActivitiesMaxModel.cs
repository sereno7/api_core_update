﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Models
{
    public class ActivitiesMaxModel
    {
        public string Name { get; set; }
        public int MaxPoint { get; set; }
    }
}
