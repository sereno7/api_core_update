﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Models
{
    public class LeaderBoardDetailsByActivityModel
    {
        public string TeamName { get; set; }
        public int IdDay { get; set; }
        public int IdTeam { get; set; }
        public int IdActivity { get; set; }
        public string ActivityName { get; set; }
        public int Result { get; set; }
        public string Color { get; set; }
    }
}
