﻿
using MAKERHUB_API_CORE.Security;
using MAKERHUB_API_CORE.Services;
using MAKERHUB_API_CORE.Validators;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BuildYourTeam.ASP.Models
{
    public partial class PersonModel : IPayload
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Role { get; set; }
        public int? IdTeams { get; set; }
        public int? IdDay { get; set; }
        [Required]
        public string PlainPassword { get; set; }
        [Required]
        public string Login { get; set; }

    }

}