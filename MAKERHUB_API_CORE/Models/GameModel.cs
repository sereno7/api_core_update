﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Models
{
    public class GameModel
    {
        public int? GameNumber { get; set; }
        public int? IdTeam { get; set; }
        public int? IdActivity { get; set; }
        public bool? IsReady { get; set; }
        public int? Result { get; set; }
    }
}
