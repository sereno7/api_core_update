﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Models.ModelUtil
{
    public class TabTeamIsReadyModel
    {
        public int Id { get; set; }
        public string  Name { get; set; }
        public string NameCurrentActivity { get; set; }
        public bool IsReady { get; set; }
        public string Color { get; set; }
    }
}
