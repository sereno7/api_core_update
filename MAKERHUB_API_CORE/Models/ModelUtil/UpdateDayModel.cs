﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Models.ModelUtil
{
    public class UpdateDayModel
    {
        public int IdS { get; set; }
        public int IdD { get; set; }
    }
}
