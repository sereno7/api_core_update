﻿using MAKERHUB_API_CORE.Validators;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BuildYourTeam.ASP.Models
{
    public class TeamsModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Color { get; set; }

        public string Slogan { get; set; }
        public int? TotalPoint { get; set; }
        [Required]
        public int IdDay { get; set; }
        public int? CurrentActivity { get; set; }
        public int? OldActivity { get; set; }
        public bool? IsReady { get; set; }
        public bool? IsRegistered { get; set; }

        public IEnumerable<PersonModel> ListPlayers { get; set; }
       
    }
}