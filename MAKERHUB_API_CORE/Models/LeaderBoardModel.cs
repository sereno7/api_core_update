﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Models
{
    public class LeaderBoardModel
    {
        public int Id { get; set; }
        public int Result { get; set; }
        public int IdActivity { get; set; }
        public int IdTeam { get; set; }
        public int IdDay { get; set; }
        public int ActivityCount { get; set; }

    }
}
