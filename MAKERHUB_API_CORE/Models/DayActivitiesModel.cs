﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Models
{
    public class DayActivitiesModel
    {
        public int Id { get; set; }
        [Required]
        public int IdDay { get; set; }
        [Required]
        public int IdActivity { get; set; }
        
    }
}
