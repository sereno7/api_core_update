﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Models
{
    public class LeaderBoardTotalByTeamModel
    {
        public string TeamName { get; set; }
        public int IdDay { get; set; }
        public int IdTeam { get; set; }
        public int Total { get; set; }
        public string Color { get; set; }
    }
}
