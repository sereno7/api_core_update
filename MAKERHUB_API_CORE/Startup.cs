using BuildYourTeam.DAL.Repositories;
using MAKERHUB_API_CORE.Security;
using MAKERHUB_API_CORE.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using signalR_testing.Hubs;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {


            services.AddCors(options =>
            {
            options.AddPolicy("CorsPolicy", builder => builder
            .SetIsOriginAllowed(_ => true)
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials());
            });

            services.AddRazorPages();
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Utc;
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd";
            });



            //Change le serializer de microsoft au profit de newtonsoft !!!!!!!!!!!!!!!!!!
            //services.AddControllers();

            //SIGNAL R
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = false,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.Configuration["jwt:Signature"]))


                };
                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        var accessToken = context.Request.Query["access_token"];

                        // If the request is for our hub...
                        var path = context.HttpContext.Request.Path;
                        if (!string.IsNullOrEmpty(accessToken) &&
                            (path.StartsWithSegments("/hub")))
                        {
                            // Read the token out of the query string
                            context.Token = accessToken;
                        }
                        return Task.CompletedTask;
                    }
                };
            });

            services.AddSignalR().AddNewtonsoftJsonProtocol();


            //string connString = Configuration.GetSection("Connection").GetSection("ConnectionString").Value;
            //string providerName = Configuration.GetSection("Connection").GetSection("ProviderName").Value;

            services.AddScoped(builder => new SqlConnection(Configuration.GetConnectionString("default")));


            //services.AddScoped((x) => new DayRepository(connString, providerName));
            //services.AddScoped((x) => new TeamsRepository(connString, providerName));
            //services.AddScoped((x) => new PersonRepository(connString, providerName));
            //services.AddScoped((x) => new ActivityRepository(connString, providerName));
            //services.AddScoped((x) => new DayActivitiesRepository(connString, providerName));
            //services.AddScoped((x) => new LeaderBoardRepository(connString, providerName));

            services.AddScoped<DayRepository>();
            services.AddScoped<TeamsRepository>();
            services.AddScoped<PersonRepository>();
            services.AddScoped<ActivityRepository>();
            services.AddScoped<DayActivitiesRepository>();
            services.AddScoped<LeaderBoardRepository>();
            services.AddScoped<LogRepository>();


            services.AddScoped<DayService>();
            services.AddScoped<TeamsService>();
            services.AddScoped<PersonService>();
            services.AddScoped<ActivityService>();
            services.AddScoped<DayActivitiesService>();
            services.AddScoped<LeaderBoardService>();
            services.AddScoped<SecurityService>();
            services.AddScoped<LogService>();

            // Singleton => Only one instance for all API
            services.AddSingleton<TimerService>();


            services.AddScoped<JwtSecurityTokenHandler>();
            services.AddScoped<JwtService>();
            //services.AddSingleton<Decrypt>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "API", Version = "v1" });
            });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "API v1"));
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseCors("CorsPolicy");


            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
                endpoints.MapHub<GameHub>("/gamehub");
            });
            app.UseAuthentication();
        }
    }
}
