﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Security
{
    public interface IPayload
    {
        int Id { get; }
        string Name { get; }
        string Role { get; }
        public int? IdTeams { get; set; }
        public int? IdDay { get; set; }
    }
}
