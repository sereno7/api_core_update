﻿using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Repositories;
using MAKERHUB_API_CORE.Controllers;
using MAKERHUB_API_CORE.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlTypes;
using BuildYourTeam.DAL.Entities;
using Serilog;

namespace MAKERHUB_API_CORE.Services
{
    public class SecurityService
    {
        
        private JwtService jwtService;
        private PersonRepository repo;
        private static List<PersonModel> personConnected = new List<PersonModel>();

        //private Decrypting _decrypting = new Decrypting();


        public SecurityService(JwtService jwtService, PersonRepository repo)
        {
            this.jwtService = jwtService;
            this.repo = repo;
        }

        public string login(LoginModel dto)
        {
            try
            {
                //string pwd = _decrypt.Decryption(dto.PlainPassword);
                //PersonModel u = repo.Login(dto.Login, pwd).Mapto<PersonModel>();
                Person p = repo.Login(dto.Login, dto.PlainPassword);
                if (!(p is null))
                {
                    PersonModel u = p.Mapto<PersonModel>();
                    personConnected.Add(u);
                    string token = jwtService.EncodeToken(u);
                    return token;
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                Log.Error(e, "TOKEN ERROR");
                throw e;
            }
        }

        public void logoff(int id)
        {
            personConnected.Remove(personConnected.Find(x => x.Id == id));
        }
    }
}
