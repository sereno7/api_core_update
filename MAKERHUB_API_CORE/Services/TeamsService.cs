﻿using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using MAKERHUB_API_CORE.Models.ModelUtil;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;


namespace MAKERHUB_API_CORE.Services
{
    public class TeamsService
    {
        private TeamsRepository repo;
        private PersonRepository repo2;

        public TeamsService(TeamsRepository repo, PersonRepository repo2)
        {
            this.repo = repo;
            this.repo2 = repo2;
        }

        public IEnumerable<TeamsModel> Get()
        {
            IEnumerable<Teams> l = repo.Get().ToList();
            return l.Select(d =>
            {
                TeamsModel r = d.Mapto<TeamsModel>();
                r.ListPlayers = repo2.Get().Where((x) => x.IdTeams == r.Id)
                    .Select(x => x.Mapto<PersonModel>());
                return r;
            });
        }

        public void Put(TeamsModel z)
        {
            TeamsModel r = repo.GetById(z.Id).Mapto<TeamsModel>();
            r.CurrentActivity = z.CurrentActivity;
            r.IsReady = z.IsReady;
            r.IsRegistered = z.IsRegistered;
            repo.Update(r.Mapto<Teams>());
        }

        public TeamsModel GetById(int id)
        {
            TeamsModel t = repo.GetById(id).Mapto<TeamsModel>();
            return t;
        }

        public IEnumerable<TabTeamIsReadyModel> GetByCurrentActivity(int id)
        {
            IEnumerable<TabTeamIsReadyModel> teams = repo.GetByCurrentActivity(id).Select(x => x.Mapto<TabTeamIsReadyModel>());
            return teams;
        } 

        public void ResetIsReady()
        {
            repo.ResetIsReady();
        }

        public void ResetIsRegistered()
        {
            repo.ResetIsRegistered();
        }

        public bool Add(TeamsModel t)
        {
            int id = repo.Insert(t.Mapto<Teams>());
            bool r = repo2.personExists(t.Name + t.IdDay);
            if (!r)
            {
                PersonModel p = new PersonModel()
                {
                    IdDay = t.IdDay,
                    IdTeams = id,
                    Name = t.Name,
                    Login = t.Name+t.IdDay,
                    PlainPassword = t.Name,
                    Role = "ANIMATOR"
                };
                repo2.Add(p.Mapto<Person>());
            }
            return r;
        }

        internal void Delete(int id)
        {
            repo.Delete(id);
        }

        public bool teamExist(string name)
        {
            return repo2.personExists(name);
        }
        public void ResetCurrentActivity()
        {
            repo.ResetCurrentActivity();
        }
    }
}

