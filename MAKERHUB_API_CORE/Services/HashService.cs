﻿using System.Security.Cryptography;
using System.Text;

namespace BuildYourTeam.ASP.Services
{
    public class HashService
    {
        private HashAlgorithm _algo;
        public HashService(HashAlgorithm algorithm)
        {
            _algo = algorithm;
        }
        public byte[] HashPassword(string password)
        {
            return _algo.ComputeHash(Encoding.UTF8.GetBytes(password));
        }
    }
}