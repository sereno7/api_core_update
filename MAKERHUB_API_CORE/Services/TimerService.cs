﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Services
{
    public class TimerService
    {
        private int _timer = 15;
        private long _pauseTotal;
        public bool roundEnd = false;

        public long PauseTotal
        {
            get { return _pauseTotal; }
        }

        public long PauseDuration
        {
            get
            {
                if (_pauseTimestamp != 0)
                    return GetTimestamp() - _pauseTimestamp;
                else
                    return 0;
            }
        }

        public int Timer
        {
            get { return _timer; }
        }

        private long _startimestamp = 0;
        public long Startimestamp
        {
            get { return _startimestamp; }
        }

        private long _pauseTimestamp;
        public long PauseTimestamp
        {
            get { return _pauseTimestamp; }
        }


        public long GetTimestamp()
        {
            return DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        }

        public void SetStartTimestamp(int? nb)
        {

            if (nb == 0)
            {
                _startimestamp = 0;
            }
            else
            {
                _startimestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            }
        }


        public void SetPauseTimestamp(int nb)
        {
            if (nb == 0)
            {
                _pauseTotal += PauseDuration;
                _pauseTimestamp = 0;
            }
            else
            {
                _pauseTimestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            }
        }

        public void ResetPauseTotal()
        {
           _pauseTotal = 0; 
        }
    }
}
