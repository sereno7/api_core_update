﻿using MAKERHUB_API_CORE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.DAL.Repositories;
using Serilog;

namespace MAKERHUB_API_CORE.Services
{
    public class DayActivitiesService
    {
        private DayActivitiesRepository repo;


        public DayActivitiesService(DayActivitiesRepository repo)
        {
            this.repo = repo;
        }

        public IEnumerable<DayActivitiesModel> GetAll()
        {

            IEnumerable<DayActivities> l = repo.Get().ToList();
            return l.Select(d =>
            {
                DayActivitiesModel r = d.Mapto<DayActivitiesModel>();
                return r;
            });

        }

        public void Add(DayActivitiesModel model)
        {
            repo.Insert(model.Mapto<DayActivities>());
        }

        public void DeleteByDetails(int idA, int idD)
        {
            repo.DeleteByDetails(idA, idD);        
        }

        public IEnumerable<DayActivitiesModel> GetByIdDay(int id)
        {
            return repo.GetByIdDay(id).Select(x => x.Mapto<DayActivitiesModel>());
        }
    }
}
