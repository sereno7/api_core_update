﻿using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.DAL.Repositories;
using MAKERHUB_API_CORE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Services
{
    public class LogService
    {
        private LogRepository repo;

        public LogService(LogRepository repo)
        {
            this.repo = repo;
        }

        public IEnumerable<LogModel> Get()
        {
            return repo.Get().Select(x => x.Mapto<LogModel>());
        }
    }
}
