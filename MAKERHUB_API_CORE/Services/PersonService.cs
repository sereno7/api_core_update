﻿using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MAKERHUB_API_CORE.Services
{
    public class PersonService
    {
        private PersonRepository repo;


        public PersonService(PersonRepository repo)
        {
            this.repo = repo;
        }

        public IEnumerable<PersonModel> Get()
        {

                IEnumerable<Person> l = repo.Get().ToList();
                return l.Select(d =>
                {
                    //PersonModel r = d.Mapto<PersonModel>();
                    //return r;

                    PersonModel r = new PersonModel
                    {
                        Id = d.Id,
                        IdDay = d.IdDay,
                        IdTeams = d.IdTeams,
                        Name = d.Name,
                        Role = d.Role
                    };
                    return r;
                });
        }

        public bool personExists(string name)
        {
           return repo.personExists(name);
        }
    }
}
