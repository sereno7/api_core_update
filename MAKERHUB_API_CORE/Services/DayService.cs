﻿using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using MAKERHUB_API_CORE.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;


namespace MAKERHUB_API_CORE.Services
{
    public class DayService
    {
        private DayRepository repo;
        private TeamsRepository repo2;
        private PersonRepository repo3;

        public DayService(DayRepository repo, TeamsRepository repo2, PersonRepository repo3)
        {
            this.repo = repo;
            this.repo2 = repo2;
            this.repo3 = repo3;
        }


        public IEnumerable<DayModel> Get()
        {
            IEnumerable<Day> l = repo.Get().ToList();
            return l.Select(d =>
            {
                DayModel r = d.Mapto<DayModel>();
                r.listTeams = repo2.Get().Where((x) => x.IdDay == r.Id)
                    .Select(x => x.Mapto<TeamsModel>()).ToList();
                return r;
            });
        }

        public void Delete(int id)
        {
            repo.Delete(id);
        }

        public DayModel GetById(int id)
        {
            DayModel d = repo.GetById(id).Mapto<DayModel>();
            d.listTeams = repo2.Get().Where((x) => x.IdDay == d.Id)
                    .Select(x => x.Mapto<TeamsModel>());
            return d;
        }

        public int Post(DayModel d)
        {
            d.Status = 1;
            int id = repo.Insert(d.Mapto<Day>());
            PersonModel p = new PersonModel()
            {
                IdDay = id,
                Name = d.CustomerName,
                IdTeams = null,
                Login = d.CustomerName + id,
                PlainPassword = d.CustomerName,
                Role = "MASTER"
            };
            repo3.Add(p.Mapto<Person>());
            return id;
        }

        public void Put(int id)
        {
            DayModel r = repo.GetById(id).Mapto<DayModel>();
            r.CountActivity++;
            repo.Update(r.Mapto<Day>());
        }

        public void Update(DayUpdateModel d)
        {
            repo.Update(d.Mapto<Day>());
        }

        public void UpdateStatus(int idD, int idS)
        {
            DayModel r = repo.GetById(idD).Mapto<DayModel>();
            r.Status = idS;
            repo.Update(r.Mapto<Day>());

        }

        public bool CheckDate(DateTime? date, int id = 0)
        {
            return repo.CheckDate(date, id);
        }

        public bool CheckDate(DateTime? date)
        {
            return repo.CheckDate(date);
        }

    }
}
