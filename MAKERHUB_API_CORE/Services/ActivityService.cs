﻿using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using MAKERHUB_API_CORE.Models;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Services
{
    public class ActivityService
    {
        private ActivityRepository repo;
        private DayActivitiesRepository repo2;


        public ActivityService(ActivityRepository repo, DayActivitiesRepository repo2)
        {
            this.repo = repo;
            this.repo2 = repo2;
        }

        public IEnumerable<ActivityModel> GetActivityOrderById()
        {

            IEnumerable<Activity> l = repo.GetActivityOrderById().ToList();
            return l.Select(d =>
            {
                ActivityModel r = d.Mapto<ActivityModel>();
                return r;
            });

        }

        public ActivityModel GetById(int id)
        {
            ActivityModel a = repo.GetById(id).Mapto<ActivityModel>();
            return a;
        }

        public void Add(ActivityModel model)
        {
            int id = repo.Insert(model.Mapto<Activity>());
            model.Id = id;
        }

        public void Delete(int id)
        {
            repo.Delete(id);
        }

        public bool activiyExist(string name)
        {
            return repo.activiyExist(name);
        }

        public bool ActivityAlreadyUsed(int id)
        {
            return repo2.ActivityAlreadyUsed(id);
        }
    }
}
