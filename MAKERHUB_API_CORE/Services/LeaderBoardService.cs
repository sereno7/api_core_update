﻿using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using MAKERHUB_API_CORE.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAKERHUB_API_CORE.Services
{
    public class LeaderBoardService
    {
        private LeaderBoardRepository repo;

        public LeaderBoardService(LeaderBoardRepository repo)
        {
            this.repo = repo;
        }

        public IEnumerable<LeaderBoardModel> GetAll()
        {
            IEnumerable<LeaderBoard> l = repo.Get().ToList();
            return l.Select(d =>
            {
                LeaderBoardModel r = d.Mapto<LeaderBoardModel>();
                return r;
            });
        }

        public IEnumerable<LeaderBoardDetailsModel> GetByIdActity(int id)
        {
            IEnumerable<LeaderBoardDetails> l = repo.GetByIdActity(id).ToList();
            return l.Select(d =>
            {
                LeaderBoardDetailsModel r = d.Mapto<LeaderBoardDetailsModel>();
                return r;
            });
        }





        public void Add(LeaderBoardModel model)
        {
            repo.Insert(model.Mapto<LeaderBoard>());

        }


        //Récupère les classements pour la current actitity(Activity count)
        public IEnumerable<LeaderBoardDetailsByActivityModel> GetLeaderboardDetailsByActivityCount(int idD, int Acount)
        {
            IEnumerable<LeaderBoardDetailsByActivityModel> l = repo.GetLeaderboardDetailsByActivityCount(idD, Acount).Select(x => x.Mapto<LeaderBoardDetailsByActivityModel>());
            return l;
        }

        //Total de point par équipe
        public IEnumerable<LeaderBoardTotalByTeamModel> GetLeaderBoardTotalByTeam(int idD)
        {
            IEnumerable<LeaderBoardTotalByTeamModel> l = repo.GetLeaderBoardTotalByTeam(idD).Select(x => x.Mapto<LeaderBoardTotalByTeamModel>());
            return l;

        }

        //Classement des TEAMS par ACTIVITY
        public IEnumerable<LeaderBoardDetailsByActivityModel> GetLeaderboardDetailsByActivity(int idD, int idA)
        {
            IEnumerable<LeaderBoardDetailsByActivityModel> l = repo.GetLeaderboardDetailsByActivity(idD, idA).Select(x => x.Mapto<LeaderBoardDetailsByActivityModel>());
            return l;
        }

        public IEnumerable<ActivitiesAverageModel> GetAverage()
        {
            return repo.GetAverage().Select(x => x.Mapto<ActivitiesAverageModel>());
        }

        public IEnumerable<ActivitiesMinModel> GetMin()
        {
            return repo.GetMin().Select(x => x.Mapto<ActivitiesMinModel>());
        }

        public IEnumerable<ActivitiesMaxModel> GetMax()
        {
            return repo.GetMax().Select(x => x.Mapto<ActivitiesMaxModel>());
        }
    }
}
