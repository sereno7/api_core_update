﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ToolBox.ADO.Utils
{
    public abstract class BaseRepository<T>
        where T : class, new()
    {
        private SqlConnection connection;

        public BaseRepository(SqlConnection connection)
        {
            //_connectionString = connectionString;
            //_providerName = providerName;
            this.connection = connection;
        }

        /// <summary>
        /// Retourne la liste des lignes à partir de "offset"
        /// Jusqu'à offset + limit
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>IEnumerable<T></returns>
        public virtual IEnumerable<T> Get(int limit = 100, int offset = 0)
        {
            try
            {
                //    connection.Open();
                //    string query = "SELECT * FROM [" + typeof(T).Name
                //        + "] ORDER BY Id DESC OFFSET @p2 ROWS FETCH NEXT @p1 ROWS ONLY";
                //    Dictionary<string, object> dico = new Dictionary<string, object>
                //        {
                //            { "@p1", limit },
                //            { "@p2", offset }
                //        };
                //    IDbCommand cmd = CreateCommand(connection, query, dico);
                //    IDataReader r = cmd.ExecuteReader();
                //    while (r.Read())
                //    {
                //        yield return ReaderToEntityMapper(r);
                //    }
                //    connection.Close();
                connection.Open();
                string query = "SELECT * FROM [" + typeof(T).Name
                    + "] ORDER BY Id DESC OFFSET @p2 ROWS FETCH NEXT @p1 ROWS ONLY";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@p1", limit },
                        { "@p2", offset }
                    };
                IDbCommand cmd = CreateCommand(connection, query, dico);
                IDataReader r = cmd.ExecuteReader();
                List<T> l = new List<T>();
                while (r.Read())
                {
                    T t = ReaderToEntityMapper(r);
                    l.Add(t);                 
                }
                connection.Close();
                return l;
            }
            catch (Exception e)
            {
                Log.Error(e, "BaseRepository ERROR GET");
                throw e;
            }

        }

        /// <summary>
        /// Retourne une instance de la ligne ciblée par l'id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>T</returns>
        public virtual T GetById(int id)
        {
            try
            {
                connection.Open();
                string query = "SELECT * FROM [" + typeof(T).Name + "] WHERE Id = @p1";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@p1", id }
                    };
                IDbCommand cmd = CreateCommand(connection, query, dico);
                IDataReader r = cmd.ExecuteReader();
                T result = null;
                if (r.Read())
                {
                    result = ReaderToEntityMapper(r);
                }
                connection.Close();
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e, "BaseRepository ERROR GetById");
                throw e;
            }
        }

        /// <summary>
        /// Insère une ligne dans la table et retourne l'Id de la ligne insérée
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual int Insert(T entity)
        {
            try
            {
                connection.Open();
                IEnumerable<PropertyInfo> props
                    = typeof(T).GetProperties();
                string query
                    = "INSERT INTO ["
                    + typeof(T).Name + "](";
                foreach (PropertyInfo p in props)
                {
                    if (p.Name != "Id")
                        query += $"[{p.Name}],";
                }
                query = query.Substring(0, query.Length - 1);
                query += ") OUTPUT INSERTED.Id VALUES(";

                foreach (PropertyInfo p in props)
                {
                    if (p.Name != "Id")
                        query += "@" + p.Name + ",";
                }
                query = query.Substring(0, query.Length - 1);
                query += ")";
                Dictionary<string, object> parameters
                    = new Dictionary<string, object>();
                foreach (PropertyInfo p in props)
                {

                    if (p.Name != "Id")
                    {

                        parameters.Add("@" + p.Name, p.GetValue(entity) ?? DBNull.Value);
                    }

                }
                IDbCommand cmd = CreateCommand(connection, query, parameters);
                int id = (int)cmd.ExecuteScalar();
                connection.Close();
                return id;
            }
            catch (Exception e)
            {
                Log.Error(e, "BaseRepository ERROR INSERT");
                throw e;
            }
        }

        /// <summary>
        /// Met à jour toutes les colonnes de la table
        /// Si elles correspondent aux propriétés de l'entité
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>bool</returns>
        public virtual bool Update(T entity)
        {
            try
            {
                connection.Open();
                string query = "UPDATE [" + typeof(T).Name + "] SET ";
                IEnumerable<PropertyInfo> props
                    = typeof(T).GetProperties();
                foreach (PropertyInfo p in props)
                {
                    if (p.Name != "Id")
                        query += $"[{p.Name}] = @{p.Name},";
                }
                query = query.Substring(0, query.Length - 1);
                query += " WHERE Id = @Id";
                Dictionary<string, object> parameters
                    = new Dictionary<string, object>();
                foreach (PropertyInfo p in props)
                {
                    parameters.Add("@" + p.Name, p.GetValue(entity) ?? DBNull.Value);
                }

                IDbCommand cmd = CreateCommand(connection, query, parameters);
                int nb = cmd.ExecuteNonQuery();
                connection.Close();
                return nb != 0;
            }
            catch (Exception e)
            {
                Log.Error(e, "BaseRepository ERROR UPDATE");
                throw e;
            }


        }
        /// <summary>
        /// Supprime la ligne correspond à l'id
        /// et retourne vrai si la ligne a été supprimée
        /// Fonctionne si la colonne de la primary key s'appelle Id,
        /// si la colonne est un int
        /// et si la table à le même nom que l'entité
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        public virtual bool Delete(int id)
        {
            try
            {
                connection.Open();
                string query = "DELETE FROM [" + typeof(T).Name + "] WHERE Id = @p1";
                Dictionary<string, object> parameters =
                    new Dictionary<string, object>()
                    {
                            { "@p1", id }
                    };
                IDbCommand cmd = CreateCommand(connection, query, parameters);
                int nbLines = cmd.ExecuteNonQuery();
                connection.Close();
                return nbLines != 0;
            }
            catch (Exception e)
            {
                Log.Error(e, "BaseRepository ERROR DELETE");
                throw e;
            }
        }

        /// <summary>
        /// Récupère la connection du répository
        /// </summary>
        /// <returns>IDbConnection</returns>
        //protected IDbConnection GetConnection()
        //{
        //    //DbProviderFactory factory = DbProviderFactories.GetFactory(_providerName);

        //    // crée ma connection
        //    IDbConnection connection = new SqlConnection();

        //    // set ma connection à ma connection
        //    connection.ConnectionString = _connectionString;

        //    return connection;
        //}

        /// <summary>
        /// Crée une commande Sql à partir d'une connection, d'une query SQL et d'une dictionnaire de paramètres
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="text"></param>
        /// <param name="parameters"></param>
        /// <returns>IDbCommand</returns>
        protected IDbCommand CreateCommand(IDbConnection conn, string text, Dictionary<string, object> parameters = null)
        {
            try
            {
                IDbCommand cmd = conn.CreateCommand();
                cmd.CommandText = text;
                if (parameters != null)
                {
                    foreach (KeyValuePair<string, object> kvp in parameters)
                    {
                        IDataParameter p = cmd.CreateParameter();
                        p.ParameterName = kvp.Key;
                        p.Value = kvp.Value;
                        cmd.Parameters.Add(p);
                    }
                }
                return cmd;
            }
            catch (Exception e)
            {
                Log.Error(e, "BaseRepository ERROR CreateCMD");
                throw e;
            }
        }

        /// <summary>
        /// Créer une Instance de T à partir d'un IDataReader
        /// Ne Fonctionne que si les colonnes de l'entité correspondent aux colonnes de la db
        /// </summary>
        /// <param name="r"></param>
        /// <returns>T</returns>
        protected T ReaderToEntityMapper(IDataReader r)
        {
            try
            {
                T s = new T();
                IEnumerable<PropertyInfo> properties = s.GetType().GetProperties();
                foreach (PropertyInfo p in properties)
                {
                    p.SetValue(s, r[p.Name] == DBNull.Value ? null : r[p.Name]);
                }
                return s;
            }
            catch (Exception e)
            {
                Log.Error(e, "BaseRepository ERROR MAPPER");
                throw e;
            }
        }
    }
}

