﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildYourTeam.DAL.Entities
{
    public class LeaderBoardDetails
    {
        public int Id { get; set; }
        public int Result { get; set; }
        public int IdActivity { get; set; }
        public int IdTeam { get; set; }
        public int IdDay { get; set; }
        public int ActivityCount { get; set; }
        public string ActivityName { get; set; }
        public string TeamName { get; set; }

    }
}
