﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildYourTeam.DAL.Entities
{
    public class ActivitiesMax
    {
        public string Name { get; set; }
        public int MaxPoint { get; set; }
    }
}
