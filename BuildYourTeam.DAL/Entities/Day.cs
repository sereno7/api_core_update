﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BuildYourTeam.DAL.Entities
{
    public class Day
    {
        public int Id { get; set; }
        [Required]
        public string CustomerName { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public int CountActivity { get; set; }
        public int Status { get; set; }
    }
}
