﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildYourTeam.DAL.Entities
{
    public class LeaderBoardDetailsByActivity
    {
        public string TeamName { get; set; }
        public int IdDay { get; set; }
        public int IdTeam { get; set; }
        public int IdActivity { get; set; }
        public string ActivityName { get; set; }
        public int Result { get; set; }
        public string Color { get; set; }
    }
}
