﻿using System.ComponentModel.DataAnnotations;

namespace BuildYourTeam.DAL.Entities
{
    public class Teams
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int? CurrentActivity { get; set; }
        public int? OldActivity { get; set; }
        public string Color { get; set; }
        public string Slogan { get; set; }
        public int? TotalPoint { get; set; }
        [Required]
        public int IdDay { get; set; }
        public bool? IsReady { get; set; }
        public bool? IsRegistered { get; set; }

    }
}
