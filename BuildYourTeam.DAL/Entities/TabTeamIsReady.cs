﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildYourTeam.DAL.Entities
{
    public class TabTeamIsReady
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameCurrentActivity { get; set; }
        public bool IsReady { get; set; }
        public string Color { get; set; }
    }
}
