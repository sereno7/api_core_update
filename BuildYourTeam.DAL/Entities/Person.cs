﻿
namespace BuildYourTeam.DAL.Entities
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? IdTeams { get; set; }
        public string Role { get; set; }
        public string PlainPassword { get; set; }
        public string Login { get; set; }
        public int? IdDay { get; set; }

        //public bool IsAlreadyConnected {get; set;}

    }
}
