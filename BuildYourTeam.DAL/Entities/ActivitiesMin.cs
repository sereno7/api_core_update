﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildYourTeam.DAL.Entities
{
    public class ActivitiesMin
    {
        public string Name { get; set; }
        public int MinPoint { get; set; }
    }
}
