﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildYourTeam.DAL.Entities
{
    public class LeaderBoardTotalByTeam
    {
        public string TeamName { get; set; }
        public int IdDay { get; set; }
        public int IdTeam { get; set; }
        public int Total { get; set; }
        public string Color { get; set; }
    }
}
