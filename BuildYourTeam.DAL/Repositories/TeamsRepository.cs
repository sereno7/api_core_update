﻿using BuildYourTeam.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ToolBox.ADO.Utils;

namespace BuildYourTeam.DAL.Repositories
{
    public class TeamsRepository : BaseRepository<Teams>
    {
        private SqlConnection _connection;
        public TeamsRepository(SqlConnection connection) : base(connection)
        {
            _connection = connection;
        }

        public IEnumerable<TabTeamIsReady> GetByCurrentActivity(int id)
        {
            _connection.Open();
            string query =
            $"SELECT id,Name,Color,isReady, NameCurrentActivity, IsRegistered FROM V_TeamsByCurrentActivity WHERE CountActivity = {id} AND CurrentActivity IS NOT NULL";
            IDbCommand cmd = CreateCommand(_connection, query);
            IDataReader r = cmd.ExecuteReader();
            List<TabTeamIsReady> a = new List<TabTeamIsReady>();
            while (r.Read())
            {
                TabTeamIsReady t = new TabTeamIsReady
                {
                    Id = (int)r["Id"],
                    Name = (string)r["Name"],
                    Color = (string)r["Color"],
                    IsReady = (bool)r["IsReady"],
                    NameCurrentActivity = (string)r["NameCurrentActivity"],
                };
                a.Add(t);
            }
            _connection.Close();
            return a;
        }

        public void ResetIsReady()
        {
            _connection.Open();
            string query = "UPDATE Teams SET isReady = 0";
            IDbCommand cmd = CreateCommand(_connection, query);
            cmd.ExecuteNonQuery();
            _connection.Close();
        }

        public void ResetIsRegistered()
        {
            _connection.Open();
            string query = "UPDATE Teams SET IsRegistered = 0";
            IDbCommand cmd = CreateCommand(_connection, query);
            cmd.ExecuteNonQuery();
            _connection.Close();
        }
        public void ResetCurrentActivity()
        {
            _connection.Open();
            string query = "UPDATE Teams SET OldActivity = CurrentActivity;";
            IDbCommand cmd = CreateCommand(_connection, query);
            cmd.ExecuteNonQuery();
            string query2 = "UPDATE Teams SET CurrentActivity = NULL;";
            IDbCommand cmd2 = CreateCommand(_connection, query2);
            cmd2.ExecuteNonQuery();
            _connection.Close();
        }

        public void Add(Teams t)
        {
            _connection.Open();
            IDbCommand cmd = _connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_AddTeam";
            var params1 = cmd.CreateParameter();
            var params2 = cmd.CreateParameter();
            params1.ParameterName = "Name";
            params1.Value = t.Name;
            params2.ParameterName = "IdDay";
            params2.Value = t.IdDay;
            cmd.Parameters.Add(params1);
            cmd.Parameters.Add(params2);
            cmd.ExecuteReader();
            _connection.Close();
        }


        public bool teamExist(string name, int id)
        {
            _connection.Open();
            string query =
            "SELECT * FROM Teams WHERE Name = @param AND IdDay = @param2";
            SqlCommand cmd = _connection.CreateCommand();
            cmd.CommandText = query;
            cmd.Parameters.AddWithValue("@param", name);
            cmd.Parameters.AddWithValue("@param2", id);
            SqlDataReader r = cmd.ExecuteReader();
            bool result = r.Read();
            _connection.Close();
            return result;
        }
    }
}
