﻿using BuildYourTeam.DAL.Entities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace BuildYourTeam.DAL.Repositories
{
    public class ActivityRepository : BaseRepository<Activity>
    {
        private SqlConnection _connection;
        public ActivityRepository(SqlConnection connection) : base(connection)
        {
            _connection = connection;
        }

        public virtual IEnumerable<Activity> GetActivityOrderById()
        {

            _connection.Open();

            string query =
            "SELECT * FROM Activity ORDER BY Id";
            IDbCommand cmd = CreateCommand(_connection, query);
            IDataReader r = cmd.ExecuteReader();
            List<Activity> a = new List<Activity>();
            while (r.Read())
            {
                Activity p = new Activity
                {
                    Id = (int)r["Id"],
                    Name = (string)r["Name"],
                    Description = (string)r["Description"]
                };
                a.Add(p);
            }
            _connection.Close();
            return a;
        }

        public bool activiyExist(string name)
        {
            _connection.Open();
            string query =
            "SELECT * FROM Activity WHERE Name = @param";
            SqlCommand cmd = _connection.CreateCommand();
            cmd.CommandText = query;
            cmd.Parameters.AddWithValue("@param", name);
            SqlDataReader r = cmd.ExecuteReader();
            bool result = r.Read();
            _connection.Close();
            return result;
        }

    }
}
