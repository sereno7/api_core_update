﻿using BuildYourTeam.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace BuildYourTeam.DAL.Repositories
{
    public class LogRepository : BaseRepository<Logg>
    {
        private SqlConnection _connection;
        public LogRepository(SqlConnection connection) : base(connection)
        {
            _connection = connection;
        }
    }
}
