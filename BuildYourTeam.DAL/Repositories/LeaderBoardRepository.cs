﻿using BuildYourTeam.DAL.Entities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace BuildYourTeam.DAL.Repositories
{
    public class LeaderBoardRepository : BaseRepository<LeaderBoard>
    {
        private SqlConnection _connection;
        public LeaderBoardRepository(SqlConnection connection) : base(connection)
        {
            _connection = connection;
        }



        public virtual IEnumerable<LeaderBoardDetailsByActivity> GetLeaderboardDetailsByActivityCount(int idD, int Acount)
        {

            _connection.Open();
            string query =
            "SELECT TeamName, ActivityName,MAX(Result) AS Result, IdActivity, IdDay,IdTeam FROM V_Leaderboard WHERE ActivityCount = @p2 AND IdDay = @p1 GROUP BY TeamName, ActivityName, IdActivity, IdDay,IdTeam";
            Dictionary<string, object> dico = new Dictionary<string, object>
                        {
                            { "@p1", idD },
                            { "@p2", Acount }
                        };
            IDbCommand cmd = CreateCommand(_connection, query, dico);
            IDataReader r = cmd.ExecuteReader();
            List<LeaderBoardDetailsByActivity> l = new List<LeaderBoardDetailsByActivity>();
            while (r.Read())
            {
                LeaderBoardDetailsByActivity a = new LeaderBoardDetailsByActivity
                {
                    TeamName = (string)r["TeamName"],
                    ActivityName = (string)r["ActivityName"],
                    Result = (int)r["Result"],
                    IdActivity = (int)r["IdActivity"],
                    IdDay = (int)r["IdDay"],
                    IdTeam = (int)r["IdTeam"]
                };
                l.Add(a);
            }
            _connection.Close();
            return l;
        }



        public virtual IEnumerable<LeaderBoardTotalByTeam> GetLeaderBoardTotalByTeam(int idD)
        {

            _connection.Open();
            string query =
            //"SELECT IdDay, IdTeam, TeamName, SUM(Result) AS TOTAL FROM  V_Leaderboard WHERE IdDay = @p1 GROUP BY TeamName, IdTeam, IdDay ORDER BY TOTAL DESC";
            "select * from V_TotalByTeam WHERE IdDay = @p1 ORDER BY Total DESC";
            Dictionary<string, object> dico = new Dictionary<string, object>
                        {
                            { "@p1", idD },
                        };
            IDbCommand cmd = CreateCommand(_connection, query, dico);
            IDataReader r = cmd.ExecuteReader();
            List<LeaderBoardTotalByTeam> l = new List<LeaderBoardTotalByTeam>();
            while (r.Read())
            {
                LeaderBoardTotalByTeam le = new LeaderBoardTotalByTeam
                {

                    TeamName = (string)r["TeamName"],
                    IdDay = (int)r["IdDay"],
                    IdTeam = (int)r["IdTeam"],
                    Total = (int)r["Total"],
                    Color = (string)r["Color"]
                };
                l.Add(le);
            }
            _connection.Close();
            return l;
        }

        public IEnumerable<object> GetMin()
        {
            _connection.Open();
            string query = "SELECT ActivityName AS[Name], MIN(Result) AS [MinPoint] FROM V_Leaderboard GROUP BY ActivityName";
            IDbCommand cmd = CreateCommand(_connection, query);
            IDataReader r = cmd.ExecuteReader();
            while (r.Read())
            {
                yield return new ActivitiesMin
                {
                    Name = (string)r["Name"],
                    MinPoint = (int)r["MinPoint"]
                };
            }
        }

        public IEnumerable<object> GetMax()
        {
            _connection.Open();
            string query = "SELECT ActivityName AS[Name], MAX(Result) AS [MaxPoint] FROM V_Leaderboard GROUP BY ActivityName";
            IDbCommand cmd = CreateCommand(_connection, query);
            IDataReader r = cmd.ExecuteReader();
            while (r.Read())
            {
                yield return new ActivitiesMax
                {
                    Name = (string)r["Name"],
                    MaxPoint = (int)r["MaxPoint"]
                };
            }
        }

        public IEnumerable<object> GetAverage()
        {
            _connection.Open();
            string query = "SELECT ActivityName AS[Name], AVG(Result) AS [AveragePoint] FROM V_Leaderboard GROUP BY ActivityName";
            IDbCommand cmd = CreateCommand(_connection, query);
            IDataReader r = cmd.ExecuteReader();
            while(r.Read())
            {
                yield return new ActivitiesAverage
                {
                    Name = (string)r["Name"],
                    AveragePoint = (int)r["AveragePoint"]
                };
            }
        }

        public virtual IEnumerable<LeaderBoardDetailsByActivity> GetLeaderboardDetailsByActivity(int idD, int idA)
        {
            _connection.Open();
            string query =
            "SELECT IdDay, IdActivity, ActivityName, IdTeam, TeamName, Color, MAX(Result) AS Result FROM V_Leaderboard WHERE IdActivity = @p2 AND IdDay = @p1 GROUP BY ActivityName, IdActivity, IdDay, IdTeam, Color, TeamName ORDER BY Result DESC";
            Dictionary<string, object> dico = new Dictionary<string, object>
                        {
                            { "@p1", idD },
                            { "@p2", idA }
                        };
            IDbCommand cmd = CreateCommand(_connection, query, dico);
            IDataReader r = cmd.ExecuteReader();
            List<LeaderBoardDetailsByActivity> l = new List<LeaderBoardDetailsByActivity>();
            while (r.Read())
            {
                LeaderBoardDetailsByActivity le = new LeaderBoardDetailsByActivity
                {
                    TeamName = (string)r["TeamName"],
                    ActivityName = (string)r["ActivityName"],
                    Result = (int)r["Result"],
                    IdActivity = (int)r["IdActivity"],
                    IdDay = (int)r["IdDay"],
                    IdTeam = (int)r["IdTeam"],
                    Color = (string)r["Color"]
                };
                l.Add(le);
            }
            _connection.Close();
            return l;
        }



        public virtual IEnumerable<LeaderBoardDetails> GetByIdActity(int id)
        {

            _connection.Open();
            string query =
            "SELECT * FROM V_Leaderboard WHERE IdActivity = @p1 ORDER BY [Result] DESC";
            Dictionary<string, object> dico = new Dictionary<string, object>
                        {
                            { "@p1", id },
                        };
            IDbCommand cmd = CreateCommand(_connection, query, dico);
            IDataReader r = cmd.ExecuteReader();
            List<LeaderBoardDetails> l = new List<LeaderBoardDetails>();
            while (r.Read())
            {
                LeaderBoardDetails le = new LeaderBoardDetails
                {
                    Id = (int)r["Id"],
                    TeamName = (string)r["TeamName"],
                    ActivityName = (string)r["ActivityName"],
                    ActivityCount = (int)r["ActivityCount"],
                    Result = (int)r["Result"],
                    IdActivity = (int)r["IdActivity"],
                    IdDay = (int)r["IdDay"],
                    IdTeam = (int)r["IdTeam"]
                };
                l.Add(le);
            }
            _connection.Close();
            return l;

        }
    }
}
