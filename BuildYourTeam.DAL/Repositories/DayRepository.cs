﻿using BuildYourTeam.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ToolBox.ADO.Utils;

namespace BuildYourTeam.DAL.Repositories
{
    public class DayRepository : BaseRepository<Day>
    {

        private SqlConnection _connection;
        public DayRepository(SqlConnection connection) : base(connection)
        {
            _connection = connection;
        }


        public bool CheckDate(DateTime? date, int id = 0)
        {
            _connection.Open();
 
            string query = "SELECT * FROM [Day] WHERE [Date] = @param AND [Id] <> @param2";

            SqlCommand cmd = _connection.CreateCommand();
            cmd.CommandText = query;
            cmd.Parameters.AddWithValue("@param", date);
            cmd.Parameters.AddWithValue("@param2", id);

            SqlDataReader r = cmd.ExecuteReader();

            bool result = r.Read();

            _connection.Close();

            return result;
        }

        public bool CheckDate(DateTime? date)
        {
            _connection.Open();

            string query = "SELECT * FROM [Day] WHERE [Date] = @param";

            SqlCommand cmd = _connection.CreateCommand();
            cmd.CommandText = query;
            cmd.Parameters.AddWithValue("@param", date);
            SqlDataReader r = cmd.ExecuteReader();
            bool result = r.Read();
            _connection.Close();
            return result;
        }
    }
}
