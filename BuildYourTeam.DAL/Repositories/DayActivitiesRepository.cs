﻿using BuildYourTeam.DAL.Entities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace BuildYourTeam.DAL.Repositories
{
    public class DayActivitiesRepository : BaseRepository<DayActivities>
    {
        private SqlConnection _connection;
        public DayActivitiesRepository(SqlConnection connection) : base(connection)
        {
            _connection = connection;
        }
        public virtual bool DeleteByDetails(int idA, int idD)
        {

            _connection.Open();
            string query = "DELETE FROM [DayActivities] WHERE IdActivity = @p1 AND IdDay = @p2";
            Dictionary<string, object> parameters =
                new Dictionary<string, object>()
                {
                            { "@p1", idA },
                            { "@p2", idD }
                };
            IDbCommand cmd = CreateCommand(_connection, query, parameters);
            int nbLines = cmd.ExecuteNonQuery();
            _connection.Close();
            return nbLines != 0;
        }

        public IEnumerable<DayActivities> GetByIdDay(int id)
        {
            _connection.Open();
            string query =
            "SELECT * FROM DayActivities WHERE IdDay = @p1";
            Dictionary<string, object> parameters =
                new Dictionary<string, object>()
                {
                    { "@p1", id },
                };
            IDbCommand cmd = CreateCommand(_connection, query, parameters);
            IDataReader r = cmd.ExecuteReader();
            List<DayActivities> a = new List<DayActivities>();
            while (r.Read())
            {
                DayActivities p = new DayActivities
                {
                    Id = (int)r["Id"],
                    IdActivity = (int)r["IdActivity"],
                    IdDay = (int)r["IdDay"]
                };
                a.Add(p);
            }
            _connection.Close();
            return a;
        }

        public bool ActivityAlreadyUsed(int id)
        {
            _connection.Open();
            string query =
            "SELECT * FROM DayActivities WHERE IdActivity = @param";
            SqlCommand cmd = _connection.CreateCommand();
            cmd.CommandText = query;
            cmd.Parameters.AddWithValue("@param", id);
            SqlDataReader r = cmd.ExecuteReader();
            bool result = r.Read();
            _connection.Close();
            return !result;
        }
    }
}

