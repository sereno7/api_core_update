﻿using BuildYourTeam.DAL.Entities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ToolBox.ADO.Utils;


namespace BuildYourTeam.DAL.Repositories
{
    public class PersonRepository : BaseRepository<Person>
    {

        private SqlConnection _connection;
        public PersonRepository(SqlConnection connection) : base(connection)
        {
            _connection = connection;
        }

        public Person Login(string login, string password)
        {
            _connection.Open();
            IDbCommand cmd = _connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Login";
            var params1 = cmd.CreateParameter();
            var params2 = cmd.CreateParameter();
            params1.ParameterName = "Login";
            params1.Value = login;
            params2.ParameterName = "PlainPassword";
            params2.Value = password;
            cmd.Parameters.Add(params1);
            cmd.Parameters.Add(params2);
            IDataReader r = cmd.ExecuteReader();
            Person result = null;
            if (r.Read())
            {
                result = new Person
                {
                    Id = (int)r["Id"],
                    Name = (string)r["Name"],
                    Login = (string)r["Login"],
                    IdTeams = r["IdTeams"] as int?,
                    IdDay = r["IdDay"] as int?,
                    Role = (string)r["Role"]
                };
            }
            _connection.Close();
            return result;
        }

        public virtual IEnumerable<Person> Get()
        {

            _connection.Open();

            string query =
            "SELECT * FROM Person";
            IDbCommand cmd = CreateCommand(_connection, query);
            IDataReader r = cmd.ExecuteReader();
            List<Person> a = new List<Person>();
            while (r.Read())
            {
                Person p = new Person
                {
                    Id = (int)r["Id"],
                    Name = (string)r["Name"],
                    IdDay = (int)r["Id"],
                    IdTeams = (int)r["Id"],
                    Role = (string)r["Role"]
                };
                a.Add(p);
            }
            _connection.Close();
            return a;

        }

        public void Add(Person p)
        {
            _connection.Open();
            IDbCommand cmd = _connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Register";
            var params1 = cmd.CreateParameter();
            var params2 = cmd.CreateParameter();
            var params3 = cmd.CreateParameter();
            var params4 = cmd.CreateParameter();
            var params5 = cmd.CreateParameter();
            var params6 = cmd.CreateParameter();
            params1.ParameterName = "Name";
            params1.Value = p.Name;
            params2.ParameterName = "IdTeams";
            if(p.IdTeams == null)
            {
                params2.Value = DBNull.Value;
            }
            else
            {
                params2.Value = p.IdTeams;

            }
            params3.ParameterName = "Role";
            params3.Value = p.Role;
            params4.ParameterName = "PlainPassword";
            params4.Value = p.Name;
            params5.ParameterName = "Login";
            params5.Value = p.Login;
            params6.ParameterName = "IdDay";
            params6.Value = p.IdDay;
            cmd.Parameters.Add(params1);
            cmd.Parameters.Add(params2);
            cmd.Parameters.Add(params3);
            cmd.Parameters.Add(params4);
            cmd.Parameters.Add(params5);
            cmd.Parameters.Add(params6);
            cmd.ExecuteNonQuery();
            _connection.Close();
        }

        //public void AddForDay(Person p)
        //{
        //    _connection.Open();
        //    IDbCommand cmd = _connection.CreateCommand();
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.CommandText = "SP_Register";
        //    var params1 = cmd.CreateParameter();
        //    var params2 = cmd.CreateParameter();
        //    var params3 = cmd.CreateParameter();
        //    var params4 = cmd.CreateParameter();
        //    var params5 = cmd.CreateParameter();
        //    var params6 = cmd.CreateParameter();
        //    params1.ParameterName = "Name";
        //    params1.Value = name;
        //    params2.ParameterName = "IdTeams";
        //    params2.Value = DBNull.Value;
        //    params3.ParameterName = "Role";
        //    params3.Value = "MASTER";
        //    params4.ParameterName = "PlainPassword";
        //    params4.Value = name;
        //    params5.ParameterName = "Login";
        //    params5.Value = name + idDay;
        //    params6.ParameterName = "IdDay";
        //    params6.Value = idDay;
        //    cmd.Parameters.Add(params1);
        //    cmd.Parameters.Add(params2);
        //    cmd.Parameters.Add(params3);
        //    cmd.Parameters.Add(params4);
        //    cmd.Parameters.Add(params5);
        //    cmd.Parameters.Add(params6);
        //    cmd.ExecuteNonQuery();
        //    _connection.Close();
        //}

        public bool personExists(string name)
        {
            _connection.Open();
            string query =
            "SELECT * FROM Person WHERE Login = @param";
            SqlCommand cmd = _connection.CreateCommand();
            cmd.CommandText = query;
            cmd.Parameters.AddWithValue("@param", name);
            SqlDataReader r = cmd.ExecuteReader();
            bool result = r.Read();
            _connection.Close();
            return result;
        }
    }
}
