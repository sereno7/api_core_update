﻿CREATE PROCEDURE SP_Register
	@Name VARCHAR(255),
	@IdTeams INT NULL,
	@Role VARCHAR(255),
	@PlainPassword VARCHAR(255),
	@Login VARCHAR(255),
	@IdDay INT NULL
AS
BEGIN
	INSERT INTO [Person]
	VALUES (@Name,@IdTeams,@Role,HASHBYTES('SHA2_512', @Name), @Name, @IdDay)
END