﻿CREATE PROCEDURE SP_Login
	@Login VARCHAR(255),
	@PlainPassword VARCHAR(255)
AS
BEGIN
	SELECT * FROM [Person]
	WHERE [Login] = @Login AND [Password] = HASHBYTES('SHA2_512', @PlainPassword)
END