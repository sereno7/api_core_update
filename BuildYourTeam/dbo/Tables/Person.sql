﻿CREATE TABLE [dbo].[Person] (
    [Id]       INT             IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (60)   NOT NULL,
    [IdTeams]  INT             NULL,
    [Role]     NVARCHAR (50)   NULL,
    [Password] VARBINARY (MAX) NULL,
    [Login]    NVARCHAR (50)   NULL,
    [IdDay]    INT             NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Day2] FOREIGN KEY ([IdDay]) REFERENCES [dbo].[Day] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Teams] FOREIGN KEY ([IdTeams]) REFERENCES [dbo].[Teams] ([Id]) ON DELETE CASCADE
);

