﻿CREATE TABLE [dbo].[LeaderBoard] (
    [Id]            INT IDENTITY (1, 1) NOT NULL,
    [Result]        INT NULL,
    [IdActivity]    INT NULL,
    [IdTeam]        INT NULL,
    [IdDay]         INT NULL,
    [ActivityCount] INT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([IdActivity]) REFERENCES [dbo].[Activity] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK__LeaderBoa__IdDay__35BCFE0A] FOREIGN KEY ([IdDay]) REFERENCES [dbo].[Day] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK__LeaderBoa__IdTea__34C8D9D1] FOREIGN KEY ([IdTeam]) REFERENCES [dbo].[Teams] ([Id]) ON DELETE CASCADE
);

