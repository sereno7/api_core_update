﻿CREATE TABLE [dbo].[Teams] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (50)  NOT NULL,
    [Color]           NVARCHAR (50)  NULL,
    [Slogan]          NVARCHAR (250) NULL,
    [TotalPoint]      INT            CONSTRAINT [DF__Teams__TotalPoin__30F848ED] DEFAULT ((0)) NULL,
    [IdDay]           INT            NULL,
    [CurrentActivity] INT            NULL,
    [isReady]         BIT            CONSTRAINT [DF__Teams__isReady__73852659] DEFAULT ((0)) NULL,
    CONSTRAINT [PK__Teams__3214EC076831D35D] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__Teams__IdDay__38996AB5] FOREIGN KEY ([IdDay]) REFERENCES [dbo].[Day] ([Id]) ON DELETE SET NULL
);


GO
CREATE Trigger TR_INSERT_DAY_ANIMATOR
ON [Teams] AFTER INSERT
AS 
DECLARE @ref VARCHAR;
DECLARE @idTeam INT;
DECLARE @idDay INT;

SELECT @ref = INSERTED.Name FROM INSERTED
SELECT @idTeam = INSERTED.id FROM INSERTED
SELECT @idDay = INSERTED.IdDay FROM INSERTED

INSERT INTO [Person] VALUES (
	@ref,
	@idTeam,
    'Animator',
	HASHBYTES('SHA2_512',@ref),
	@ref,
	@idDay
	)
GO
DISABLE TRIGGER [dbo].[TR_INSERT_DAY_ANIMATOR]
    ON [dbo].[Teams];


GO
CREATE Trigger TR_INSERT_DAY_ANIMATOR2
ON [Teams] AFTER UPDATE
AS 
DECLARE @ref VARCHAR;
DECLARE @idTeam INT;
DECLARE @idDay INT;

SELECT @ref = INSERTED.Name FROM INSERTED
SELECT @idTeam = INSERTED.id FROM INSERTED
SELECT @idDay = INSERTED.IdDay FROM INSERTED

INSERT INTO [Person] VALUES (
	@ref,
	@idTeam,
    'Animator',
	HASHBYTES('SHA2_512',@ref),
	@ref,
	@idDay
	)


GO
DISABLE TRIGGER [dbo].[TR_INSERT_DAY_ANIMATOR2]
    ON [dbo].[Teams];

