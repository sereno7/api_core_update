﻿CREATE TABLE [dbo].[Day] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [CustomerName]  NVARCHAR (50) NOT NULL,
    [Date]          DATE          NOT NULL,
    [CountActivity] INT           CONSTRAINT [DF__Day__CountActivi__4E53A1AA] DEFAULT ((1)) NULL,
    [Status]        INT           CONSTRAINT [DF_Day_Status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK__Day__3214EC07EEC09FED] PRIMARY KEY CLUSTERED ([Id] ASC)
);

