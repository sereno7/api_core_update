﻿CREATE TABLE [dbo].[DayActivities] (
    [Id]         INT IDENTITY (1, 1) NOT NULL,
    [IdDay]      INT NOT NULL,
    [IdActivity] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Activity] FOREIGN KEY ([IdActivity]) REFERENCES [dbo].[Activity] ([Id]),
    CONSTRAINT [FK_DAY] FOREIGN KEY ([IdDay]) REFERENCES [dbo].[Day] ([Id]) ON DELETE CASCADE
);

