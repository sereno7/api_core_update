﻿CREATE TABLE [dbo].[StatusTeam]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[IdDay] INT Not NULL,
	[IdTeams] INT Not NULL,
	[IsRegistered] BIT DEFAULT ((0)),
	[IsReady] BIT DEFAULT ((0)),
	CONSTRAINT [FK_Day3] FOREIGN KEY ([IdDay]) REFERENCES [dbo].[Day] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Teams3] FOREIGN KEY ([IdTeams]) REFERENCES [dbo].[Teams] ([Id]) ON DELETE CASCADE
)
